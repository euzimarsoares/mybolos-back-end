
package br.com.MyBolos.exec;

import br.com.MyBolos.Model.cadastro.Cliente;
import br.com.MyBolos.Model.cadastro.Pessoa;
import br.com.MyBolos.Model.cadastro.Telefone;
import br.com.MyBolos.Model.cadastro.dao.BaseDAO;
import br.com.MyBolos.Model.cadastro.dao.ClienteDAO;
import br.com.MyBolos.Model.cadastro.dao.ComplementoDAO;
import br.com.MyBolos.Model.cadastro.dao.PessoaDAO;
import br.com.MyBolos.Model.cadastro.dao.ProdutoDAO;
import br.com.MyBolos.Model.cadastro.dao.TelefoneDAO;
import br.com.MyBolos.Model.cadastro.dao.TipoComplementoDAO;
import br.com.MyBolos.Model.movimento.dao.MovimentoDAO;
import com.google.gson.Gson;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import spark.Request;
import spark.Spark;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.post;
import static spark.Spark.put;


public class Rotas {
    
    public Rotas () {
        startRotas();
    }
    
    public void startRotas(){
        Spark.options("/*", (req,res) -> {
            String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                    res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                    res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }
            
            return "OK";
        });
        Spark.before((req,res) -> {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "*");
            res.type("application/json");
        });
        
        get("/", (req,res) -> {
            return "OLÁ MUNDO";
        });
        
        path("/api", () -> {
            path("/pessoa", () -> {
                post("/add", (req,res) -> addPessoa(req));
            });
            path("/cliente", () -> {
                post("/add", (req,res) -> addCliente(req));
                post("/addTeste", (req,res) -> addClienteTeste(req));
            });
            path("/administrador", () -> {
                
            });
            path("/produto", () -> {
                ProdutoDAO proDao = new ProdutoDAO();
                post("/add", (req,res) -> proDao.addProduto(req));
                get("/listar", (req,res) -> proDao.listarProdutos());
                get("/listarComplementos/:id", (req,res) -> proDao.listarComplementos(req));
                put("/update", (req,res) -> proDao.updateProdutos(req));
                put("/delete", (req,res) -> proDao.deleteProdutos(req));
            });
            path("/complemento", () -> {
                ComplementoDAO compDao = new ComplementoDAO();
                post("/add", (req,res) -> compDao.addComplemento(req));
                get("/listar", (req,res) -> compDao.listarComplemento());
                put("/update", (req,res) -> compDao.updateComplemento(req));
                put("/delete", (req,res) -> compDao.deleteComplemento(req));
            });
            path("/tipo-complemento", () -> {
                TipoComplementoDAO tipoProdDAO = new TipoComplementoDAO();
                post("/add", (req,res) -> null);
                get("/listar", (req,res) -> tipoProdDAO.listarTipoComplemento());
                put("/update", (req,res) -> null);
                put("/delete", (req,res) -> null);
            });
            path("/pedido", () -> {
                MovimentoDAO movDAO = new MovimentoDAO();
                post("/add", (req,res) -> movDAO.addMovimento(req));
                get("/listar", (req,res) -> movDAO.listarMovimentoTotal());
                put("/update", (req,res) -> null);
                put("/delete", (req,res) -> null);
            });
        });
    }
    
    public String addPessoa(Request req){
            Gson gson = new Gson();
            PessoaDAO peDAO = new PessoaDAO();
            Map<String,Object> jsonNodes = gson.fromJson(req.body(), Map.class);
            String dados = jsonNodes.get("Dados").toString();
            Pessoa pe = gson.fromJson(dados,Pessoa.class);
            peDAO.insert(pe);
        return pe.toString();
    }
    
    public String addCliente(Request req) throws NoSuchAlgorithmException, UnsupportedEncodingException{
            System.out.println("");
            Gson gson = new Gson();
            BaseDAO baseDao = new BaseDAO();
            TelefoneDAO telDao;
            ClienteDAO cliDao = new ClienteDAO();
            PessoaDAO peDao = new PessoaDAO();
            Telefone telefone;
            Pessoa pessoa;
            Cliente cliente;
            Map<String,Object> jsonNodes = gson.fromJson(req.body(), Map.class);
            String dados = jsonNodes.get("Dados").toString();
            Map<String,Object> js = gson.fromJson(dados, Map.class);
            String p = gson.toJson(js.get("pessoa"));
            String t = gson.toJson(js.get("telefone"));
            String c = gson.toJson(js.get("cliente"));
            cliente = gson.fromJson(c, Cliente.class);
            pessoa = gson.fromJson(p, Pessoa.class);
            peDao.insert(pessoa);
            List<Telefone> telList = gson.fromJson(t, List.class);
            for (int i = 0; i < telList.size();i++) {
                telefone = gson.fromJson(gson.toJson(telList.get(i)), Telefone.class);
                telefone.setPessoa(pessoa);
                telDao = new TelefoneDAO();
                telDao.insert(telefone);
            }
            cliente.setId_pessoa(pessoa);
            cliente.setSenha(baseDao.criptografy(cliente.getSenha()));
            cliDao.insert(cliente);
        return "";
    }
    
    public String addClienteTeste(Request req) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        Gson gson = new Gson();
        BaseDAO baseDao = new BaseDAO();
        TelefoneDAO telDao = new TelefoneDAO();
        ClienteDAO cliDao = new ClienteDAO();
        PessoaDAO peDao = new PessoaDAO();
        Telefone telefone = new Telefone();
        Pessoa pessoa = new Pessoa();
        Cliente cliente = new Cliente();
        System.out.println(req.body());
        Map<String,Object> jsonNodes = gson.fromJson(req.body(), Map.class);
        pessoa = gson.fromJson(jsonNodes.get("pessoa").toString(), Pessoa.class);
        cliente = gson.fromJson(jsonNodes.get("cliente").toString(), Cliente.class);
        List<Telefone> telList = gson.fromJson(jsonNodes.get("telefone").toString(), List.class);
        peDao.insert(pessoa);
        for (int i = 0; i < telList.size();i++) {
            telefone = gson.fromJson(gson.toJson(telList.get(i)), Telefone.class);
            telefone.setPessoa(pessoa);
            telDao = new TelefoneDAO();
            telDao.insert(telefone);
        }
        cliente.setId_pessoa(pessoa);
        cliente.setSenha(baseDao.criptografy(cliente.getSenha()));
        cliDao.insert(cliente);
        return "";
    }
}
