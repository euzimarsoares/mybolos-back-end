
package br.com.MyBolos.exec;

import br.com.MyBolos.Model.cadastro.dao.BaseDAO;

//classe de inicialização com o sistema
public final class StartAplication {
    
    //Construtor
    public StartAplication(){
        StartBD();
    }
    
    //Analisar e Criar Base de Dados Inexistênte
    public void StartBD(){
        BaseDAO dao = new BaseDAO();
        dao.getEntidade();
    }
    
}
