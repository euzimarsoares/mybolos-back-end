
package br.com.MyBolos.Model.movimento;

import br.com.MyBolos.Model.cadastro.Pessoa;
import br.com.MyBolos.genericDomain.genericDomain;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.annotations.Expose;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Classe Movimento

@SuppressWarnings("serial")
@Entity(name = "movimento")
@Table(name = "Movimento")
//@JsonIdentityInfo(
//  generator = ObjectIdGenerators.PropertyGenerator.class, 
//  property = "id")
public class Movimento extends genericDomain{
    //atributos
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PESSOA", nullable = false)
    @Expose
    private Pessoa id_pessoa;
    @Column(length = 200, nullable = false)
    @Expose
    private String descricao;
    @Column(scale = 3, precision = 12, nullable = false)
    @Expose
    private Double precoCusto;
    @Column(scale = 3, precision = 12, nullable = false)
    @Expose
    private Double precoVenda;
    @Column(nullable = false)
    @Expose
    private String data;
    @Column(length = 1, nullable = false)
    @Expose
    private String status;
    @Column(length = 1, nullable = false)
    @Expose
    private String tipoMovimento;
    @Expose
    @OneToMany(cascade = CascadeType.PERSIST,mappedBy = "id_movimento", fetch = FetchType.EAGER)
    private List<ProdutoVenda> produtosVenda;
    //métodos de retorno

    public Pessoa getId_pessoa() {
        return id_pessoa;
    }

    public String getDescricao() {
        return descricao;
    }

    public Double getPrecoCusto() {
        return precoCusto;
    }

    public Double getPrecoVenda() {
        return precoVenda;
    }

    public String getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getTipoMovimento() {
        return tipoMovimento;
    }

    public List<ProdutoVenda> getProdutosVenda() {
        return produtosVenda;
    }
    
    //métodos de entrada

    public void setId_pessoa(Pessoa id_pessoa) {
        this.id_pessoa = id_pessoa;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setPrecoCusto(Double precoCusto) {
        this.precoCusto = precoCusto;
    }

    public void setPrecoVenda(Double precoVenda) {
        this.precoVenda = precoVenda;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTipoMovimento(String tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }
    
    public void setProdutosVenda(List<ProdutoVenda> produtosVenda) {
        this.produtosVenda = produtosVenda;
    }
    
    //método toString

    @Override
    public String toString() {
        return "Movimento{" + "id_pessoa=" + id_pessoa + ", descricao=" + descricao + ", precoCusto=" + precoCusto + ", precoVenda=" + precoVenda + ", data=" + data + ", status=" + status + ", tipoMovimento=" + tipoMovimento + '}';
    }
    
}
