
package br.com.MyBolos.Model.movimento;

import br.com.MyBolos.Model.cadastro.Complemento;
import br.com.MyBolos.genericDomain.genericDomain;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.annotations.Expose;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Classe ComplementoVenda

@SuppressWarnings("serial")
@Entity
@Table(name = "ComplementoVenda")
//@JsonIdentityInfo(
//  generator = ObjectIdGenerators.PropertyGenerator.class, 
//  property = "id")
public class ComplementoVenda extends genericDomain{
    //atributos
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private ProdutoVenda id_produtoVenda;
    @Expose
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Complemento id_complemento;
    
    //métodos de retorno

    public ProdutoVenda getId_produtoVenda() {
        return id_produtoVenda;
    }

    public Complemento getId_complemento() {
        return id_complemento;
    }
    
    //métodos de entrada

    public void setId_produtoVenda(ProdutoVenda id_produtoVenda) {
        this.id_produtoVenda = id_produtoVenda;
    }

    public void setId_complemento(Complemento id_complemento) {
        this.id_complemento = id_complemento;
    }
    
    //método toString

    @Override
    public String toString() {
        return "ComplementoVenda{" + "id_produtoVenda=" + id_produtoVenda + ", id_complemento=" + id_complemento + '}';
    }
    
    
    
}
