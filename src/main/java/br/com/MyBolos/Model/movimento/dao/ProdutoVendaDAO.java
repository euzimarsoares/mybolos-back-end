
package br.com.MyBolos.Model.movimento.dao;

import br.com.MyBolos.Model.cadastro.dao.BaseDAO;
import br.com.MyBolos.Model.movimento.ProdutoVenda;

//Classe ProdutoVendaDAO

public class ProdutoVendaDAO {
    //Atributos
    BaseDAO base = null;
    
    //Contrstutor
    public ProdutoVendaDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar ProdutoVenda
    public void insert(ProdutoVenda prv){
        MovimentoDAO mov = new MovimentoDAO();
        mov.insert(prv.getId_movimento());
        base.insert(prv);
    }
    
    //Atualizar ProdutoVenda
    public void update(ProdutoVenda prv){
        MovimentoDAO mov = new MovimentoDAO();
        mov.update(prv.getId_movimento());
        base.update(prv);
    }
    
    //Deletar ProdutoVenda
    public void delete(ProdutoVenda prv){
        base.delete(prv, prv.getId());
    }
}
