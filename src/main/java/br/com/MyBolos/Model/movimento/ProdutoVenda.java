
package br.com.MyBolos.Model.movimento;

import br.com.MyBolos.Model.cadastro.Produto;
import br.com.MyBolos.genericDomain.genericDomain;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.annotations.Expose;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//classe ProdutoVenda

@SuppressWarnings("serial")
@Entity
@Table(name = "ProdutoVenda")
@JsonIdentityInfo(
  generator = ObjectIdGenerators.PropertyGenerator.class, 
  property = "id")
public class ProdutoVenda extends genericDomain{
    //atributos
    @Expose
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Produto id_produto;
    
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Movimento id_movimento;
    @Expose
    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "id_produtoVenda")
    private List<ComplementoVenda> complementosVenda;
    //métodos de retorno

    public Produto getId_produto() {
        return id_produto;
    }

    public Movimento getId_movimento() {
        return id_movimento;
    }

    public List<ComplementoVenda> getComplementosVenda() {
        return complementosVenda;
    }
    
    //método de entrada

    public void setId_produto(Produto id_produto) {
        this.id_produto = id_produto;
    }

    public void setId_movimento(Movimento id_movimento) {
        this.id_movimento = id_movimento;
    }

    public void setComplementosVenda(List<ComplementoVenda> complementosVenda) {
        this.complementosVenda = complementosVenda;
    }

    //método toString

    @Override
    public String toString() {
        return "ProdutoVenda{" + "id_produto=" + id_produto + ", id_movimento=" + id_movimento + '}';
    }
    
}
