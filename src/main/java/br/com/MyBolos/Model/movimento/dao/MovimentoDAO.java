
package br.com.MyBolos.Model.movimento.dao;

import br.com.MyBolos.Model.cadastro.Complemento;
import br.com.MyBolos.Model.cadastro.Pessoa;
import br.com.MyBolos.Model.cadastro.dao.BaseDAO;
import br.com.MyBolos.Model.cadastro.dao.ComplementoDAO;
import br.com.MyBolos.Model.cadastro.dao.PessoaDAO;
import br.com.MyBolos.Model.movimento.ComplementoVenda;
import br.com.MyBolos.Model.movimento.Movimento;
import br.com.MyBolos.Model.movimento.ProdutoVenda;
import br.com.MyBolos.classes.Gson.Gson_func;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.codehaus.plexus.util.StringUtils;
import spark.Request;

//Classe MovimentoDAO

public class MovimentoDAO {
    //Atributos
    BaseDAO base = null;
    
    //Contrstutor
    public MovimentoDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar Movimento
    public void insert(Movimento mov){
        base.insert(mov);
    }
    
    //Atualizar Movimento
    public void update(Movimento mov){
        base.update(mov);
    }
    
    //Deletar Movimento
    public void delete(Movimento mov){
        base.delete(mov, mov.getId());
    }
    
    //Transformar em JSON
    public String jsonTransform(List<Movimento> comp){
        Gson_func gson = new Gson_func();
        return gson.createJSON(comp);
    }
    
    //buscar todos os dados
    public String buscarDados() throws JsonProcessingException{
        List<Movimento> dados = base.buscarDados("movimento");
        System.out.println("TESTE::"+dados);
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return  gson.toJson(dados);
    }
    
    
    public String listarMovimentoTotal() throws JsonProcessingException{
        String result = buscarDados();
        System.out.println("Dados Buscados Movimento!");
        System.out.println(result);
        return result;
    }
    
    
    public boolean addMovimento(Request req){
        Gson gson = new Gson();
        Movimento mov = new Movimento();
        ComplementoVendaDAO cmpvDAO = new ComplementoVendaDAO();
        System.out.println("CHEGO AQUI");
        System.out.println(req.body());
        
        mov = gson.fromJson(req.body(), Movimento.class);
        System.out.println("DADOOOSSS::: "+mov);
        for (ProdutoVenda produtoVenda : mov.getProdutosVenda()) {
            Movimento mo = mov;
            mo.setProdutosVenda(null);
            System.out.println("Produto venda::: "+produtoVenda);
            produtoVenda.setId_movimento(mo);
            for (ComplementoVenda complementoVenda : produtoVenda.getComplementosVenda()) {
                ProdutoVenda pv = produtoVenda;
                System.out.println("COMPLEMENTO VENDA::: "+complementoVenda);
                pv.setComplementosVenda(null);
                complementoVenda.setId_produtoVenda(pv);
                cmpvDAO.insert(complementoVenda);
            }
        }
        return true;
    }
}
