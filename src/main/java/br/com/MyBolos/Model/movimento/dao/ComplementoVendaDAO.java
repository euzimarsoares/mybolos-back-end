
package br.com.MyBolos.Model.movimento.dao;

import br.com.MyBolos.Model.cadastro.dao.BaseDAO;
import br.com.MyBolos.Model.movimento.ComplementoVenda;

//Classe ComplementoVendaDAO

public class ComplementoVendaDAO {
    
    BaseDAO base = null;

    public ComplementoVendaDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar ComplementoVenda
    public void insert(ComplementoVenda cpv){
        ProdutoVendaDAO prv = new ProdutoVendaDAO();
        prv.insert(cpv.getId_produtoVenda());
        base.insert(cpv);
    }
    
    //Atualizar ComplementoVenda
    public void update(ComplementoVenda cpv){
        ProdutoVendaDAO prv = new ProdutoVendaDAO();
        prv.update(cpv.getId_produtoVenda());
        base.update(cpv);
    }
    
    //Deletar ComplementoVenda
    public void delete(ComplementoVenda cpv){
        base.delete(cpv, cpv.getId());
    }
    
}
