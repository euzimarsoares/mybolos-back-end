
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.genericDomain.genericDomain;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//classe cliente

@SuppressWarnings("serial")
@Entity
@Table(name = "cliente")
public class Cliente extends genericDomain{
    //atributos
    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_PESSOA", nullable = false)
    private Pessoa id_pessoa;
    @Column(length = 100, nullable = false)
    private String senha;
    
    //métodos de retorno
    
    public String getSenha() {
        return senha;
    }

    public Pessoa getId_pessoa() {
        return id_pessoa;
    }
    
    //métodos de entrada

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setId_pessoa(Pessoa id_pessoa) {
        this.id_pessoa = id_pessoa;
    }
    
    //método toString

    @Override
    public String toString() {
        return "Cliente{" + "id_pessoa=" + id_pessoa + ", senha=" + senha + '}';
    }
    
}
