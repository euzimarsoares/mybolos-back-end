
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.genericDomain.genericDomain;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.annotations.Expose;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//classe complemento

@SuppressWarnings("serial")
@Entity(name = "complemento")
@Table(name = "complemento")
//@JsonIdentityInfo(
//  generator = ObjectIdGenerators.PropertyGenerator.class, 
//  property = "id")
public class Complemento extends genericDomain{
    //atributos
    @Expose
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PRODUTO", nullable = false)
    private Produto id_produto;
    @Expose
    @Column(length = 50, nullable = false)
    private String nome;
    @Expose
    @Column(length = 200, nullable = true)
    private String detalhes;
    @Expose
    @Column(scale = 3, precision = 12, nullable = false)
    private BigDecimal valor;
    @Expose
    @Column(length = 3, nullable = false)
    private String TipoUnidade;
    @Expose
    @Column(columnDefinition = "Decimal(12,3) not null default 0")
    private BigDecimal quantidade;
    @Expose
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private TipoComplemento id_tipoComplemento;
    
    //métodos de retorno

    public String getNome() {
        return nome;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public String getTipoUnidade() {
        return TipoUnidade;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public Produto getId_produto() {
        return id_produto;
    }

    public TipoComplemento getId_tipoComplemento() {
        return id_tipoComplemento;
    }
    
    //métodos de entrada

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public void setTipoUnidade(String TipoUnidade) {
        this.TipoUnidade = TipoUnidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public void setId_produto(Produto id_produto) {
        this.id_produto = id_produto;
    }

    public void setId_tipoComplemento(TipoComplemento id_complemento) {
        this.id_tipoComplemento = id_complemento;
    }
    
    //método toString

    @Override
    public String toString() {
        return "Complemento{" + "id_produto=" + id_produto + ", nome=" + nome + ", detalhes=" + detalhes + ", valor=" + valor + ", TipoUnidade=" + TipoUnidade + ", quantidade=" + quantidade + '}';
    }
    
    
    

}
