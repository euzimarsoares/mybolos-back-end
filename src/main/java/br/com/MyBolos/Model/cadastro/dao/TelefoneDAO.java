
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Telefone;

//Classe TelefoneDAO
public class TelefoneDAO {
    //atributos
    BaseDAO base = null;
    
    //Construtor
    public TelefoneDAO(){
       base = new BaseDAO();
    }
    
    //cadastrar telefone
    public void insert(Telefone tel){
        base.insert(tel);
    }
    
    //Atualizar Telefone
    public void update(Telefone tel){
       base.update(tel);
    }
    
    //Deletar Telefone
    public void delete(Telefone tel){
        base.delete(tel, tel.getId());
    }
}
