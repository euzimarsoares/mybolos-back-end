
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Endereco;

//Classe EnderecoDAO
public class EnderecoDAO {
    //Atributos
    BaseDAO base = null;
    
    //Construtor
    public EnderecoDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar Endereço
    public void insert(Endereco end){
        base.insert(end);
    }
    
    //Atualizar Endereço
    public void update(Endereco end){
       base.update(end);
    }
    
    //Deletar Endereço
    public void delete(Endereco end){
        base.delete(end, end.getId());
    }
}
