
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Pessoa;
import java.util.List;

//classe Pessoa do banco de dados
public class PessoaDAO {
    //Atributos
    BaseDAO base = null;
    
    //Contrstutor
    public PessoaDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar pessoa
    public void insert(Pessoa pes){
        base.insert(pes);
    }
    
    //Atualizar pessoa
    public void update(Pessoa pes){
        base.update(pes);
    }
    
    //Deletar pessoa
    public void delete(Pessoa pes){
        base.delete(pes, pes.getId());
    }
    
    
    public String buscarCPF(String cpf){
        return base.buscarCPF(cpf, Pessoa.class);
    }
    
    public Object buscarID(Long id){
        return base.buscarID(id, "pessoa");
    }
}
