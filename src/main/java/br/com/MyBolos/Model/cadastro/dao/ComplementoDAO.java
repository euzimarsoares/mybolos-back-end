
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Complemento;
import br.com.MyBolos.Model.cadastro.Produto;
import br.com.MyBolos.Model.cadastro.TipoComplemento;
import br.com.MyBolos.classes.Gson.Gson_func;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.Map;
import spark.Request;

//Classe ComplementoDAO
public class ComplementoDAO {
    //Atributos
    BaseDAO base = null;
    
    //Construtor
    public ComplementoDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar Complemento
    public void insert(Complemento com){
        base.insert(com);
    }
    
    //Atualizar Complemento
    public void update(Complemento com){
        ProdutoDAO pro = new ProdutoDAO();
        pro.update(com.getId_produto());
        base.update(com);
    }
    
    //Deletar Complemento
    public void delete(Complemento com){
        base.delete(com, com.getId());
    }
    
    //Transformar em JSON
    public String jsonTransform(List<Complemento> comp){
        Gson_func gson = new Gson_func();
        return gson.createJSON(comp);
    }
    
    //Buscar todos os dados do banco
    public String buscarDados() throws JsonProcessingException{
        List<Complemento> dados = base.buscarDados("complemento");
//        for (Complemento dado : dados) {
//            dado.setId_produto(null);
//        }
        System.out.println("DADOS DA BUSCA TENTATIVA DE TIRAR O ID_PRODUTO");
        System.out.println(dados);
        System.out.println("");
//        new ObjectMapper().writeValueAsString(dados)
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return  gson.toJson(dados);
    }
    
    //<-------------------ROTAS-------------------->
    public boolean addComplemento(Request req){
        Gson gson = new Gson();
        Complemento comp = new Complemento();
        ProdutoDAO proDao = new ProdutoDAO();
        TipoComplementoDAO TcomplementoDAO = new TipoComplementoDAO();
        ComplementoDAO comDao = new ComplementoDAO();
        Map<String,Object> jsonNodes = gson.fromJson(req.body(), Map.class);
        
        Long idProd = Long.parseLong(jsonNodes.get("idProd").toString());
        Long idTipoComplemento = Long.parseLong(jsonNodes.get("tipoComplemento").toString());
        System.out.println("TIPO DE COMPLEMENTO");
        System.out.println(Long.parseLong(jsonNodes.get("tipoComplemento").toString()));
        System.out.println("");
        System.out.println(req.body());
        comp = gson.fromJson(req.body(), Complemento.class);
        comp.setId_produto((Produto) proDao.buscarID(idProd));
        comp.setId_tipoComplemento((TipoComplemento) TcomplementoDAO.buscarID(idTipoComplemento));
        System.out.println("CADASTRO COMPLETO COMPLEMENTO!");
        System.out.println(comp);
        comDao.insert(comp);
        return true;
    }
    
    public String listarComplemento() throws JsonProcessingException{
        ComplementoDAO comDao = new ComplementoDAO();
        System.out.println("");
        System.out.println("ESSE É O RETORNO");
        System.out.println(comDao.buscarDados());
        return comDao.buscarDados();
    }
    
    public boolean updateComplemento(Request req){
        Gson gson = new Gson();
        ComplementoDAO comDao = new ComplementoDAO();
        ProdutoDAO proDao = new ProdutoDAO();
        TipoComplementoDAO TcomplementoDAO = new TipoComplementoDAO();
        
        Map<String,Object> jsonNodes = gson.fromJson(req.body(), Map.class);
        System.out.println("");
        System.out.println(jsonNodes);
        Long idProd = Long.parseLong(jsonNodes.get("idProd").toString());
        Long idCompl = Long.parseLong(jsonNodes.get("tipoComplemento").toString());
        try {
            Complemento comp = gson.fromJson(req.body(), Complemento.class);
            comp = gson.fromJson(req.body(), Complemento.class);
            comp.setId_produto((Produto) proDao.buscarID(idProd));
            comp.setId_tipoComplemento((TipoComplemento) TcomplementoDAO.buscarID(idCompl));
            comDao.update(comp);
            System.out.println("SUCESSO");
            return true;
        } catch (Exception e) {
            System.out.println("DEU ERRO");
            return false;
        }
    }
    
    public boolean deleteComplemento(Request req){
        Gson gson = new Gson();
        ComplementoDAO comDao = new ComplementoDAO();
        System.out.println("");
        System.out.println(req.body());
        System.out.println("");
        try {
            Complemento comp = gson.fromJson(req.body(), Complemento.class);
            comDao.delete(comp);
            System.out.println("SUCESSO");
            return true;
        } catch (Exception e) {
            System.out.println("DEU ERRO");
            return false;
        }
    }
}
