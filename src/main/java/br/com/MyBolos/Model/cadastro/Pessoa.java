
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.Model.movimento.Movimento;
import br.com.MyBolos.genericDomain.genericDomain;
import com.google.gson.annotations.Expose;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//classe pessoa
@SuppressWarnings("serial")
@Entity(name = "pessoa")
@Table(name = "pessoa")
public class Pessoa extends genericDomain{
    //atributos
    @Expose
    @Column(length = 50, nullable = true)
    private String nome;
    @Expose
    @Column(length = 80, nullable = true)
    private String sobrenome;
    @Expose
    @Column(unique = true,length = 40, nullable = true)
    private String email;
    @Expose
    @Column(nullable = true)
    private String data_nascimento;
//    @JoinColumn(name = "ID_ENDERECO", referencedColumnName = "ID", nullable = true)
//    private Endereco endereco;
    @Expose
    @Column(length = 10, nullable = true)
    private String sexo;
//    @JoinColumn(name = "ID_TELEFONE", referencedColumnName = "ID", nullable = true)
//    private Telefone telefone;
    @Expose
    @Column(unique = true,length = 11, nullable = true)
    private String cpf;
    
    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, mappedBy = "id_pessoa")
    private List<Movimento> movimentoList;
    //Métodos de retorno
    
    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public String getData_nascimento() {
        return data_nascimento;
    }


    public String getSexo() {
        return sexo;
    }
    
    public String getCpf() {
        return cpf;
    }

    public Movimento getMovimentoList() {
        return (Movimento) movimentoList;
    }

    
    //Métodos de entrada

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setData_nascimento(String data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public void setMovimentoList(Movimento movimentoList) {
        this.movimentoList = (List<Movimento>) movimentoList;
    }


    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

//    public void setEndereco(Endereco endereco) {
//        this.endereco = endereco;
//    }
//
//    public void setTelefone(Telefone telefone) {
//        this.telefone = telefone;
//    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    
    
    //Método toString

    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + ", sobrenome=" + sobrenome + ", email=" + email + ", data_nascimento=" + data_nascimento + ", sexo=" + sexo + ", cpf=" + cpf + '}';
    }

    

    
    
    
}
