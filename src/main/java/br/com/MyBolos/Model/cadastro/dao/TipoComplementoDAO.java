/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Complemento;
import br.com.MyBolos.classes.Gson.Gson_func;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;

/**
 *
 * @author EUZIMAR
 */
public class TipoComplementoDAO {
    
    //Atributos
    BaseDAO base = null;
    
    public TipoComplementoDAO() {
         base = new BaseDAO();
    }
    
    public String jsonTransform(List<Complemento> compl){
        Gson_func gson = new Gson_func();
        return gson.createJSON(compl);
    }
    
    public String buscarDados(){
        System.out.println("<<<BUSCANDO DADOS COMPLEMENTO TIPO>>>");
        List<Complemento> dados = base.buscarDados("tipo_complemento");
        System.out.println("DADOS: "+dados);
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return  gson.toJson(dados);
    }
    
    public String listarTipoComplemento(){
        TipoComplementoDAO complDAO = new TipoComplementoDAO();
        return complDAO.buscarDados();
    }
    
    //buscar tipo complemento pelo id
    public Object buscarID(Long id){
        System.out.println("BUSCANDO ID COMPLEMENTO");
        return base.buscarID(id, "tipo_complemento");
    }
}
