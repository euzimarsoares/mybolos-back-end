
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Complemento;
import br.com.MyBolos.Model.cadastro.Produto;
import br.com.MyBolos.classes.Gson.Gson_func;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import spark.Request;

//Classe ProdutoDAO
public class ProdutoDAO {
    //Atributos
    BaseDAO base = null;
    
    //Construtor
    public ProdutoDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar Produto
    public void insert(Produto pro){
        base.insert(pro);
    }
    
    //Atualizar Produto
    public void update(Produto pro){
       base.update(pro);
    }
    
    //Deletar Produto
    public void delete(Produto pro){
        base.delete(pro, pro.getId());
    }
    
    //Transformar em Json
    public String jsonTransform(List<Produto> pro){
        Gson_func gson = new Gson_func();
        return gson.createJSON(pro);
    }
    //Transformar em Json
    public String jsonTransformComplemento(List<Complemento> pro){
        Gson_func gson = new Gson_func();
        return gson.createJSON(pro);
    }
    
    //buscar todos os dados do banco
    public String buscarDados() throws JsonProcessingException{
        List<Produto> dados = base.buscarDados("Produto");
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return  gson.toJson(dados);
    }
    
    public Object buscarID(Long id){
        return base.buscarID(id,"Produto");
    }
    
    //<-------------------ROTAS--------------------->
    
    public boolean addProduto(Request req){
        Gson gson = new Gson();
        ProdutoDAO prodDao = new ProdutoDAO();
        try {
            Produto prod = gson.fromJson(req.body(), Produto.class);
            prodDao.insert(prod);
            System.out.println("SUCESSO");
            return true;
        } catch (Exception e) {
            System.out.println("DEU ERRO");
            return false;
        }
    }
    
    public String listarProdutos() throws JsonProcessingException{
        ProdutoDAO proDao = new ProdutoDAO();
        System.out.println("");
        System.out.println("ESSE É O RETORNO");
        System.out.println(proDao.buscarDados());
        return proDao.buscarDados(); 
    }
    
    public boolean updateProdutos(Request req){
        Gson gson = new Gson();
        ProdutoDAO prodDao = new ProdutoDAO();
        try {
            Produto prod = gson.fromJson(req.body(), Produto.class);
            prodDao.update(prod);
            System.out.println("SUCESSO");
            return true;
        } catch (Exception e) {
            System.out.println("DEU ERRO");
            return false;
        }
    }
    
    public boolean deleteProdutos(Request req){
        Gson gson = new Gson();
        ProdutoDAO prodDao = new ProdutoDAO();
        System.out.println("");
        System.out.println(req.body());
        System.out.println("");
        try {
            Produto prod = gson.fromJson(req.body(), Produto.class);
            prodDao.delete(prod);
            System.out.println("SUCESSO");
            return true;
        } catch (Exception e) {
            System.out.println("DEU ERRO");
            return false;
        }
    }
    
    public String listarComplementos(Request req){
        Long id = Long.parseLong(req.params(":id"));
        Produto prod = new Produto();
        ProdutoDAO prodDAO = new ProdutoDAO();
        prod = (Produto) prodDAO.buscarID(id);
        System.out.println(prod);
        System.out.println("");
        for (Complemento complemento : prod.getId_complemento()) {
            complemento.setId_produto(null);
        }
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(prod.getId_complemento());
    }
    
}
