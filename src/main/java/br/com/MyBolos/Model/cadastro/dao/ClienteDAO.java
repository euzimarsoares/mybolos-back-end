
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Cliente;

//Classe ClienteDAO
public class ClienteDAO {
    //atributos
    BaseDAO base = null;
    
    //Construtor
    public ClienteDAO(){
       base = new BaseDAO();
    }
    
    //cadastrar Cliente
    public void insert(Cliente cli){
        base.insert(cli);
    }
    
    //Atualizar Cliente
    public void update(Cliente cli){
       PessoaDAO pes = new PessoaDAO();
       pes.update(cli.getId_pessoa());
       base.update(cli);
    }
    
    //Deletar Cliente
    public void delete(Cliente cli){
        base.delete(cli, cli.getId());
    }
}
