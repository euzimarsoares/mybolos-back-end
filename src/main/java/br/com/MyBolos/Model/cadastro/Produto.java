
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.Model.movimento.ProdutoVenda;
import br.com.MyBolos.genericDomain.genericDomain;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.annotations.Expose;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//classe produto

@SuppressWarnings("serial")
@Entity()
@Table(name = "produto")
@JsonIdentityInfo(
  generator = ObjectIdGenerators.PropertyGenerator.class, 
  property = "id")
public class Produto extends genericDomain{
    //atributos
    @Column(length = 50, nullable = false, unique = true)
    @Expose
    private String nome;
    @Column(length = 200, nullable = true)
    @Expose
    private String detalhes;
    @Column(scale = 3, precision = 12, nullable = false)
    @Expose
    private BigDecimal preco;
    @Column(length = 3, nullable = false)
    @Expose
    private String tipoUnidade;
    @Column(nullable = false, columnDefinition = "Decimal(12,3) default 0")
    @Expose
    private BigDecimal quantidade;
   @OneToMany(cascade = CascadeType.REFRESH,mappedBy = "id_produto", fetch = FetchType.EAGER)
    private List<Complemento> id_complemento;
    
    @OneToMany(cascade = CascadeType.REFRESH, mappedBy = "id_produto", fetch = FetchType.EAGER)
    private List<ProdutoVenda> id_produtoVenda;
    
    //métodos de retorno

    public String getNome() {
        return nome;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public String getTipoUnidade() {
        return tipoUnidade;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public List<Complemento> getId_complemento() {
        return id_complemento;
    }

    public List<ProdutoVenda> getId_produtoVenda() {
        return id_produtoVenda;
    }
    
    //métodos de entrada

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }
    
    public void setTipoUnidade(String TipoUnidade) {
        this.tipoUnidade = TipoUnidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }
    
    public void setId_complemento(List<Complemento> id_complemento) {
        this.id_complemento = id_complemento;
    }
    
    public void setId_produtoVenda(List<ProdutoVenda> id_produtoVenda) {
        this.id_produtoVenda = id_produtoVenda;
    }
    //método toString

    @Override
    public String toString() {
        return "Produto{" + "nome=" + nome + ", detalhes=" + detalhes + ", preco=" + preco + ", TipoUnidade=" + tipoUnidade + ", quantidade=" + quantidade + '}';
    }
    
}
