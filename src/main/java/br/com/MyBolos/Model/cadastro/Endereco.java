
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.genericDomain.genericDomain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

//classe endereco

@SuppressWarnings("serial")
@Entity
@Table(name = "endereco")
public class Endereco extends genericDomain{
    //atributos
    @Column(length = 80, nullable = false)
    private String rua;
    @Column(length = 40, nullable = false)
    private String bairro;
    @Column(length = 80, nullable = true)
    private String complemento;
    @Column(length = 20, nullable = false)
    private String numero;
    @Column(length = 10, nullable = false)
    private String cep;
    @Column(length = 40, nullable = false)
    private String cidade;
    @JoinColumn(name = "ID_PESSOA", referencedColumnName = "ID", nullable = true)
    private Pessoa pessoa;
    //métodos de retorno

    public String getRua() {
        return rua;
    }

    public String getBairro() {
        return bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getNumero() {
        return numero;
    }

    public String getCidade() {
        return cidade;
    }

    public String getCep() {
        return cep;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }
    
    //métodos de entrada

    public void setRua(String rua) {
        this.rua = rua;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
    //método toString

    @Override
    public String toString() {
        return "Endereco{" + "rua=" + rua + ", bairro=" + bairro + ", complemento=" + complemento + ", numero=" + numero + ", cep=" + cep + ", cidade=" + cidade + ", pessoa=" + pessoa + '}';
    }
    
}
