/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.genericDomain.genericDomain;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.google.gson.annotations.Expose;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author EUZIMAR
 */
@SuppressWarnings("serial")
@Entity(name = "tipo_complemento")
@Table(name = "tipo_complemento")   
@JsonIdentityInfo(
  generator = ObjectIdGenerators.PropertyGenerator.class, 
  property = "id")
public class TipoComplemento extends genericDomain{
    @Expose
    @Column(length = 50, nullable = false)
    private String tipo;
    @OneToMany(cascade = CascadeType.REFRESH,mappedBy = "id_tipoComplemento", fetch = FetchType.EAGER)
    private List<Complemento> id_complemento;

    //Métodos Get
    
    public String getTipo() {
        return tipo;
    }

    public List<Complemento> getId_complemento() {
        return id_complemento;
    }
    
    //Métodos Set

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setId_complemento(List<Complemento> id_complemento) {
        this.id_complemento = id_complemento;
    }
    
}
