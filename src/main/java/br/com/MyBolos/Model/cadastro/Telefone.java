
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.genericDomain.genericDomain;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//classe Telefone

@SuppressWarnings("serial")
@Entity
@Table(name = "telefone")  
public class Telefone extends genericDomain{
    //atributos
    @Column(length = 3, nullable = false)
    private String ddd;
    @Column(length = 10, nullable = false)
    private String numero;
    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ID_PESSOA", nullable = true)
    private Pessoa pessoa;

    //métodos de retorno

    public String getDdd() {
        return ddd;
    }

    public String getNumero() {
        return numero;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }
    
    //métodos de entrada

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
    
    //método toString

    @Override
    public String toString() {
        return "Telefone{" + "ddd=" + ddd + ", numero=" + numero + ", pessoa=" + pessoa + '}';
    }

    
    
    
    
    
}
