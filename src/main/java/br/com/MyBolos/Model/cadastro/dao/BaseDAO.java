
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Pessoa;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.eclipse.persistence.config.QueryType;


//classe BaseDAO
public class BaseDAO {
    //atributos
    private EntityManagerFactory emf = null;
    
    //construtor
    public BaseDAO(){
        emf = Persistence.createEntityManagerFactory("mybolosDB");
    }
    
    //Criar entidades
    public EntityManager getEntidade(){
        return emf.createEntityManager();
    }
    
    //Inserir dados
    public boolean insert(Object obj){
        EntityManager em = getEntidade();
        try {
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            return true;
        } catch(Exception e){
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        } 
    }
    
    //Atualizar dados
    public boolean update(Object obj){
        EntityManager em = getEntidade();
        try {
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch(Exception e){
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        } 
    }
    
    //Remover dados
    public boolean delete(Object obj, Long id){
        EntityManager em = getEntidade();
        try {
            em.getTransaction().begin();
            em.remove(em.getReference(obj.getClass(), id));
            em.getTransaction().commit();
            return true;
        } catch(Exception e){
            em.getTransaction().rollback();
            return false;
        } finally {
            em.close();
        } 
    }
    
    //Pegar uma dado do banco pelo id
    public Object buscarID(Long id, String tabela){
        System.out.println("ENTROU NO BUSCAR ID");
                
        EntityManager em = getEntidade();
        try {
            Query query = em.createQuery("select p from "+tabela+" p where p.id="+id);
            return query.getSingleResult();
        } catch(Exception e){
            System.out.println("ERRORR>>> "+e);
            return null;
        } finally {
            em.close();
        }
    }
    
    //buscar todos os dados da tabela
    public List buscarDados(String tabela){
        EntityManager em = getEntidade();
        try {
            Query query = em.createQuery("select p from "+tabela+" p");
//            List pro = query.getResultList();
            return query.getResultList();
        } catch(Exception e){
            System.out.println(e);
            return null;
        } finally {
            em.close();
        }
    }
    
    //Pegar uma dado do banco pelo id
    public String buscarCPF(String cpf, Object obj){
        EntityManager em = getEntidade();
        try {
            Query query = em.createNativeQuery("select * from pessoa as pe where pe.cpf = "+cpf);
            Object result = query.getSingleResult();
            return result.toString().intern();
        } catch(Exception e){
            System.out.println("");
            System.out.println("ERRO! "+e);
            return null;
        } finally {
            em.close();
        }
    }
    
    //Criptografia de senhas
    public String criptografy(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException{
        MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
            byte messageDigestSenhaAdmin[] = algorithm.digest(senha.getBytes("UTF-8"));

            StringBuilder hexStringSenhaAdmin = new StringBuilder();
            for (byte b : messageDigestSenhaAdmin) {
                     hexStringSenhaAdmin.append(String.format("%02X", 0xFF & b));
            }
            
        return hexStringSenhaAdmin.toString();
    }
}
