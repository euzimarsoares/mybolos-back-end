
package br.com.MyBolos.Model.cadastro;

import br.com.MyBolos.genericDomain.genericDomain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

//classe administrador

@SuppressWarnings("serial")
@Entity
@Table(name = "administrador")
public class Administrador extends genericDomain{
    //atributos
    @JoinColumn(name = "ID_PESSOA", referencedColumnName = "ID", nullable = false)
    private Pessoa id_pessoa;
    @Column(length = 15, nullable = false)
    private String senha;   

    //métodos de retorno
    
    public Pessoa getId_pessoa() {
        return id_pessoa;
    }

    public String getSenha() {
        return senha;
    }
    
    //métodos de entrada

    public void setId_pessoa(Pessoa id_pessoa) {
        this.id_pessoa = id_pessoa;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    //métodos toString

    @Override
    public String toString() {
        return "Administrador{" + "id_pessoa=" + id_pessoa + ", senha=" + senha + '}';
    }
    
    
    
    
}
