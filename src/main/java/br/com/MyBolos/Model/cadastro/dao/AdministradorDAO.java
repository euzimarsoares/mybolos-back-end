
package br.com.MyBolos.Model.cadastro.dao;

import br.com.MyBolos.Model.cadastro.Administrador;

//classe adimistrador do banco de dados
public class AdministradorDAO {
    //Atributos
    BaseDAO base = null;
    
    //Construtor
    public AdministradorDAO() {
        base = new BaseDAO();
    }
    
    //Cadastrar Administrador
    public void insert(Administrador adm){
        PessoaDAO pes = new PessoaDAO();
        pes.insert(adm.getId_pessoa());
        base.insert(adm);
    }
    
    //Atualizar Administrador
    public void update(Administrador adm){
        PessoaDAO pes = new PessoaDAO();
        pes.update(adm.getId_pessoa());
       base.update(adm);
    }
    
    //Deletar Administrador
    public void delete(Administrador adm){
        base.delete(adm, adm.getId());
    }
}
