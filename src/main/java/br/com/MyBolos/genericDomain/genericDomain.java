
package br.com.MyBolos.genericDomain;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

//Classe de domínio Genérico
@SuppressWarnings("serial")
@MappedSuperclass
public class genericDomain implements Serializable{
    //atributos
    @Expose
    @Id
    @Column(name = "ID",length = 8, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //Métodos de retorno
    public Long getId() {
        return id;
    }

    //Métodos de entrada
    public void setId(Long id) {
        this.id = id;
    }
    
    
}
