
package br.com.MyBolos.testes;

import br.com.MyBolos.Model.cadastro.Endereco;
import br.com.MyBolos.Model.cadastro.Pessoa;
import br.com.MyBolos.Model.cadastro.Telefone;
import br.com.MyBolos.Model.cadastro.dao.EnderecoDAO;
import br.com.MyBolos.Model.cadastro.dao.PessoaDAO;
import br.com.MyBolos.Model.cadastro.dao.TelefoneDAO;
import br.com.MyBolos.exec.StartAplication;
import java.time.LocalDate;

public class TesteSistema {
    public static void main(String[] args) {
        
        StartAplication start = new StartAplication();
        Telefone tel = new Telefone();
        Endereco end = new Endereco();
        Pessoa pe = new Pessoa();
        PessoaDAO peDAO = new PessoaDAO();
        TelefoneDAO teDAO = new TelefoneDAO();
        EnderecoDAO endDAO = new EnderecoDAO();
        
        tel.setId(Long.parseLong("1"));
        tel.setDdd("38");
        tel.setNumero("997332759");
        
        end.setId(Long.parseLong("1"));
        end.setBairro("Bairro");
        end.setCep("38680000");
        end.setCidade("Arinos");
        end.setComplemento("casa");
        end.setNumero("123");
        end.setRua("RUA");
        
        pe.setId(Long.parseLong("1"));
        pe.setNome("Euzimar");
        pe.setCpf("13702127631");
        pe.setData_nascimento("01/05/1998");
        pe.setEmail("euzimarsoares@gmail.com");
        pe.setSexo("M");
        pe.setSobrenome("F S");
        
        
        
        endDAO.insert(end);
        peDAO.insert(pe);
        
    }
    
}
